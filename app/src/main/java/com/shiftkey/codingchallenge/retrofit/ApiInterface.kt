package com.shiftkey.codingchallenge.retrofit


import com.shiftkey.codingchallenge.model.Data
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("available_shifts")
    fun getShifts(@Query("address") address: String, @Query("start") start: String, @Query("type") type: String = "week"): Call<Data>

}