package com.shiftkey.codingchallenge.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shiftkey.codingchallenge.model.Shift
import com.shiftkey.codingchallenge.repository.MainActivityRepository
import java.text.SimpleDateFormat
import java.util.*

class ShiftListViewModel : ViewModel() {

    var servicesLiveData: MutableLiveData<List<Shift>>? = null
    var currentDate: Long = 0

    var formatter = SimpleDateFormat("yyyy-MM-dd")

    fun fetchNextShifts() : LiveData<List<Shift>>? {
        if (currentDate == 0L) {
            currentDate = System.currentTimeMillis()
        } else {
            currentDate += 7 * 24 * 60 * 60 * 1000
        }
        val dateStr = formatter.format(Date(currentDate))
        return getShifts(dateStr)
    }

    fun getShifts(startDate: String) : LiveData<List<Shift>>? {
        servicesLiveData = MainActivityRepository.getShiftsApiCall(startDate)
        return servicesLiveData
    }

}