package com.shiftkey.codingchallenge.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.shiftkey.codingchallenge.model.Data
import com.shiftkey.codingchallenge.model.Shift
import com.shiftkey.codingchallenge.retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

object MainActivityRepository {

    val shiftList = MutableLiveData<List<Shift>>()

    fun getShiftsApiCall(startDate: String): MutableLiveData<List<Shift>> {

        val call = RetrofitClient.apiInterface.getShifts("Dallas,TX", startDate)

        call.enqueue(object: Callback<Data> {
            override fun onFailure(call: Call<Data>, t: Throwable) {

                Log.v("DEBUG : ", t.message.toString())
            }

            override fun onResponse(
                call: Call<Data>,
                response: Response<Data>
            ) {

                Log.v("DEBUG : ", response.body().toString())

                val data = response.body()

                val fullShiftList = ArrayList<Shift>()
                for (shiftList in data!!.data) {
                    fullShiftList.addAll(shiftList.shifts)
                }

                shiftList.value = fullShiftList
            }
        })

        return shiftList
    }
}