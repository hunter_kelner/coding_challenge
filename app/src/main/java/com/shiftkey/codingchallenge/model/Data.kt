package com.shiftkey.codingchallenge.model

data class Data (
    val data: List<ShiftsForDate>
)