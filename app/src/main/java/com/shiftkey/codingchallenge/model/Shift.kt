package com.shiftkey.codingchallenge.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Shift(
    val shift_id: Int,
    val start_time: String,
    val end_time: String,
    val normalized_start_date_time: String,
    val normalized_end_date_time: String,
    val timezone: String,
    val premium_rate: Boolean,
    val covid: Boolean,
    val shift_kind: String,
    val within_distance: Int,
    val facility_type: FacilityType,
    val skill: Skill,
    val localized_specialty: LocalizedSpecialty
)  : Parcelable