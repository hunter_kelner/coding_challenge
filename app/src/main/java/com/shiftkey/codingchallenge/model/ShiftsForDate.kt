package com.shiftkey.codingchallenge.model

data class ShiftsForDate (
    val date: String,
    val shifts: List<Shift>
)