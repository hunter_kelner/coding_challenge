package com.shiftkey.codingchallenge.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Skill (
    val id: Int,
    val name: String,
    val color: String
) : Parcelable