package com.shiftkey.codingchallenge.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LocalizedSpecialty (
    val id: Int,
    val specialty_id: String,
    val state_id: Int,
    val name: String,
    val abbreviation: String,
    val specialty: Specialty
) : Parcelable