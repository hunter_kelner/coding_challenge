package com.shiftkey.codingchallenge.util

import java.text.SimpleDateFormat

class DateFormatter {

    companion object {
        fun reformat(initialDate: String) : String {
            val initialFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val displayFormat = SimpleDateFormat("HH:mm dd-MM-yyyy")

            val date = initialFormat.parse(initialDate)
            return displayFormat.format(date)
        }
    }
}