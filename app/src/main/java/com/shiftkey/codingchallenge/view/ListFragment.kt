package com.shiftkey.codingchallenge.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shiftkey.codingchallenge.R
import com.shiftkey.codingchallenge.recyclerview.ShiftAdapter
import com.shiftkey.codingchallenge.recyclerview.ShiftAdapter.OnItemClickListener
import com.shiftkey.codingchallenge.viewmodel.ShiftListViewModel

class ListFragment : Fragment() {


    lateinit var shiftListViewModel: ShiftListViewModel

    var isLoading = false
    lateinit var adapter: ShiftAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false);
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val recyclerView = view.findViewById<RecyclerView>(R.id.shiftRecyclerView)

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && !isLoading) {
                    isLoading = true
                    shiftListViewModel.fetchNextShifts()
                }
            }
        })
        adapter = ShiftAdapter(ArrayList(), object : OnItemClickListener {
            override fun onItemClick(position: Int) {
                val shift = adapter.items[position]
                val action = ListFragmentDirections.actionListFragmentToDetailsFragment(shift)
                findNavController().navigate(action)
            }
        })

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)


        shiftListViewModel = ViewModelProvider(this)[ShiftListViewModel::class.java]

        shiftListViewModel.fetchNextShifts()!!.observe(viewLifecycleOwner, Observer { shiftList ->
                isLoading = false
                adapter.addList(shiftList)

            }
        )

    }
}