package com.shiftkey.codingchallenge.view

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.shiftkey.codingchallenge.R
import com.shiftkey.codingchallenge.util.DateFormatter
import kotlinx.android.synthetic.main.fragment_details.*
import com.shiftkey.codingchallenge.model.Shift as Shift

class DetailsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val shift = requireArguments().getParcelable<Shift>("shift")
        if (shift != null) {
            shiftStartTimeTxt.text = String.format(getString(R.string.start_date), DateFormatter.reformat(shift.normalized_start_date_time))
            shiftEndTimeTxt.text = String.format(getString(R.string.end_date), DateFormatter.reformat(shift.normalized_end_date_time))
            shiftTimezoneTxt.text = String.format(getString(R.string.timezone) , shift.timezone)
            if (shift.premium_rate) {
                shiftPremiumTxt.visibility = View.VISIBLE
            } else {
                shiftPremiumTxt.visibility = View.GONE
            }
            shiftKindTxt.text = String.format(getString(R.string.shift_kind), shift.shift_kind)
            shiftFacilityTxt.text = String.format(getString(R.string.facility), shift.facility_type.name)
            shiftFacilityTxt.setTextColor(Color.parseColor(shift.facility_type.color))
            shiftSkillTxt.text = String.format(getString(R.string.skill), shift.skill.name)
            shiftSkillTxt.setTextColor(Color.parseColor(shift.skill.color))
            shiftSpecialtyTxt.text = String.format(getString(R.string.specialty), shift.localized_specialty.name)
            shiftSpecialtyTxt.setTextColor(Color.parseColor(shift.localized_specialty.specialty.color))
        }
    }
}