package com.shiftkey.codingchallenge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shiftkey.codingchallenge.recyclerview.ShiftAdapter
import com.shiftkey.codingchallenge.viewmodel.ShiftListViewModel

class MainActivity : AppCompatActivity() {

    lateinit var mainActivityViewModel: ShiftListViewModel

    var isLoading = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }
}