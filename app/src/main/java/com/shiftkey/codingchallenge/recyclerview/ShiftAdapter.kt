package com.shiftkey.codingchallenge.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.shiftkey.codingchallenge.R
import com.shiftkey.codingchallenge.model.Shift
import android.content.Intent
import com.shiftkey.codingchallenge.util.DateFormatter


class ShiftAdapter(var items: ArrayList<Shift>, var listener: OnItemClickListener) :
    RecyclerView.Adapter<ShiftAdapter.ShiftViewHolder>() {

    fun addList(data: List<Shift>) {
        val oldCount = items.size
        items.addAll(data)
        notifyItemRangeChanged(oldCount, items.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShiftViewHolder {
        return ShiftViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: ShiftViewHolder, position: Int) {
        val current = items[position]
        holder.bind(current)
        holder.itemView.setOnClickListener {
            listener.onItemClick(position)
        }
    }

    class ShiftViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val startTimeTxt: TextView = itemView.findViewById(R.id.startTimeTxt)
        private val facilityNameTxt: TextView = itemView.findViewById(R.id.facilityNameTxt)

        fun bind(item: Shift) {
            startTimeTxt.text = DateFormatter.reformat(item.normalized_start_date_time)
            facilityNameTxt.text = item.facility_type.name
        }

        companion object {
            fun create(parent: ViewGroup): ShiftViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.recyclerview_item, parent, false)
                return ShiftViewHolder(view)
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }


    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

}