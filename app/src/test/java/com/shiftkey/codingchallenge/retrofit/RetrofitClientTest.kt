package com.shiftkey.codingchallenge.retrofit

import org.junit.Test

class RetrofitClientTest {

    @Test
    fun `api is called with the right parameters`() {

        val call =  RetrofitClient.apiInterface.getShifts("Dallas,TX", "2021-11-11").request()

        assert(call.method == "GET")
        assert(call.url.toString() == "https://staging-app.shiftkey.com/api/v2/available_shifts?address=Dallas%2CTX&start=2021-11-11&type=week")

    }

}